from alook_tools import init_db_connection, init_db_catalog
import pandas as pd

host_config = {
     'host': '127.0.0.1',
     'port': 3306,
     'user': 'rfw_user',
     'password': 'rfw_energos_43718',
     'database_type': 'MySQL'}

sentiments = ['neg', 'pos']

# to be replaced by a list from a query
population = '2656'

connection = init_db_connection(host_config)

query_summarization_queries = '''
SELECT * FROM erfw.odyssey_summarization_library;
'''
summarization_queries_df = connection.exec_data_frame(query_summarization_queries)

constructs = summarization_queries_df['construct']
queries = summarization_queries_df['query']

for construct, query in zip(constructs, queries):

    for sentiment in sentiments:

        comments_construct_sentiment = connection.exec_data_frame(query.format(sentiment=sentiment,
                                                                               population=population))
        print(construct)
        print(comments_construct_sentiment)

        # PLACE FOR THE SUMMARIZER

        # PLACE TO WRITE TO erfw.odyssey_test_comments_harvested