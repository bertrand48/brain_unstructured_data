#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 10:40:25 2020

@author: bertrandbuisson
"""

import pandas as pd
from sqlalchemy.engine.url import URL
from sqlalchemy import create_engine, String, Column, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

data = pd.read_csv('/home/bertrand/brain_unstructured_data/odyssey_unstructured_scoring_library.csv', delimiter=',')

HOST = '127.0.0.1'
PORT = 3306
DRIVER_NAME = 'mysql'
DATABASE_TYPE = 'MySQL'
PLATO_SCHEMA_NAME = 'erfw'

DB_USER = 'rfw_user'
DB_PASSWORD = 'rfw_energos_43718'

Base = declarative_base()


class unstructured_sl(Base):
    __tablename__ = 'odyssey_unstructured_scoring_library'
    id = Column(Integer, primary_key=True)
    question_code = Column(Integer)
    rank = Column(Integer)
    subquestion_code = Column(Integer)
    action = Column(String(256))


class Db:

    def __init__(self):
        host_config = {
            'drivername': DRIVER_NAME,
            'username': DB_USER,
            'password': DB_PASSWORD,
            'host': HOST,
            'port': PORT,
            'database': PLATO_SCHEMA_NAME}

        self.engine = create_engine(URL(**host_config))
        self.Session = sessionmaker(bind=self.engine)
        unstructured_sl.__table__.create(bind=self.engine, checkfirst=True)

    def get_session(self):
        return self.Session()

    def get_connection(self):
        return self.engine.connect()


db = Db()
session = db.get_session()

for i in range(len(data)):
    line = data.iloc[i]
    session.add(unstructured_sl(
        question_code=line['question_code'],
        rank=line['rank'],
        subquestion_code=0,  # line['subquestion_code'],
        action=line['action'],
        )
    )
    session.commit()
session.close()
