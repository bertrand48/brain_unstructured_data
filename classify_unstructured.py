from alook_tools import init_db_connection, init_db_catalog
from plato_classification.light_classifier_faster import LightClassifierFast
import pandas as pd
import ast

"""
fetch text responses for current cycle
apply required action (nudge, MB classification)
writes back in the tables

IS THERE A NEED TO FILTER BY CLIENT / CYCLE???
"""

BATCH_SIZE = 8


def reformat_df(input_df):

    output_columns = ['question_code', 'response', 'rank', 'id_user_coach',
                      'id_response_detail', 'construct_type',
                      'construct_label', 'sentiment_label',
                      'construct_probability',
                      'sentiment_probability']

    output_df = pd.DataFrame(columns=output_columns)
    for index, line in input_df.iterrows():
        response = line['response']
        id_user_coach = int(line['id_user_coach'])
        rank = int(line['RANK'])
        id_response_detail = int(line['id_response_detail'])
        question_code = line['question_code']
        construct = line['construct_type']
        sentiment = line['sentiment_labels']
        sentiment_proba = line['sent_probabilities']
        if line['construct_labels']:
            for label, label_proba in zip(line['construct_labels'],
                                          line['construct_probabilities']):

                list_to_include = [question_code, response, rank,
                                   id_user_coach, id_response_detail,
                                   construct, label, sentiment, label_proba,
                                   sentiment_proba]
                tmp_df = pd.DataFrame([list_to_include], columns=output_columns)
                output_df = output_df.append(tmp_df, ignore_index=True)

    return output_df


# host_config.read(host_config_file)
# host_config = configparser.ConfigParser()

root_path_models = '/home/bertrand/plato-database/trained_models/'
MB_MODEL_PATH = root_path_models + 'MB/MB51_PhoenixViper_USE4_best_model_all_data_06042020.h5'
SENT_MODEL_PATH = root_path_models + 'SENT/SENT_MB_Nudge_best_params_all_data_24032020.h5'
NUDGE_MODEL_PATH = root_path_models + 'NUDGE/Multi_Nudge_USE4_best_params_all_data_24032020.h5'
LOG_PATH = '/home/bertrand/logs/'

try:

    lc = LightClassifierFast(path_mb_model=MB_MODEL_PATH,
                             path_sent_model=SENT_MODEL_PATH,
                             path_nudge_model=NUDGE_MODEL_PATH,
                             path_log=LOG_PATH,
                             text_column='response')

    # host_config = dict(host_config_from_file._sections('db'))
    # host_config['port'] = int(host_config['port'])
    host_config = {
     'host': '127.0.0.1',
     'port': 3306,
     'user': 'rfw_user',
     'password': 'rfw_energos_43718',
     'database_type': 'MySQL'}

    # connection = init_db_connection(host_config._sections['db'])
    connection = init_db_connection(host_config)
    catalog = init_db_catalog(connection=connection, database_type='MySQL')
    classified_table_exists = catalog.check_table_existence(schema_name='erfw', table_name='odyssey_test_responses_classified')
    if not classified_table_exists:
        catalog.create_table(schema_name='erfw',
                             table_name='odyssey_test_responses_classified',
                             table_columns=[{'column_name': 'question_code',
                                        'data_type': 'INTEGER'},
                                        {'column_name': 'response',
                                        'data_type': 'TEXT'},
                                        {'column_name': 'rank',
                                        'data_type': 'INTEGER'},
                                        {'column_name': 'id_user_coach',
                                        'data_type': 'INTEGER'},
                                        {'column_name': 'id_response_detail',
                                        'data_type': 'INTEGER'},
                                        {'column_name': 'construct_type',
                                        'data_type': 'TEXT'},
                                        {'column_name': 'construct_label',
                                        'data_type': 'TEXT'},
                                        {'column_name': 'sentiment_label',
                                        'data_type': 'TEXT'},
                                        {'column_name': 'construct_probability',
                                        'data_type': 'FLOAT'},
                                        {'column_name': 'sentiment_probability',
                                        'data_type': 'FLOAT'}])

    list_questions_request = '''
    SELECT distinct od.question_code, sq.RANK
        FROM erfw.odyssey_test_responses od
    left join business_area.SUBQUESTION sq on od.ID_SUBQUESTION = sq.ID_SUBQUESTION
        where question_type_code='INSTRUCTION';
    '''
    list_of_questions = connection.exec_data_frame(list_questions_request)
    questions = list_of_questions['question_code']
    ranks = list_of_questions['RANK']

    for i in range(len(questions)):
        questions[i] = int(questions[i])
        ranks[i] = int(ranks[i])

    # GET "unstructured_scoring_library"
    scoring_library_request = '''
    SELECT
        *
    FROM
        erfw.odyssey_unstructured_scoring_library;
    '''
    scoring_library = connection.exec_data_frame(scoring_library_request)
    print(scoring_library)

    # LOOP OVER QUESTIONS
    for question, rank in zip(questions, ranks):

        print('starting classifications for question {0}, rank {1}'.format(question, rank))
        sl_line_q = scoring_library.loc[scoring_library['question_code'] == question].reset_index(drop=True)
        sl_line = sl_line_q.loc[sl_line_q['rank'] == rank].reset_index(drop=True)
        actions = ast.literal_eval(sl_line['action'][0])

        # at the moment we don't do anything for animals
        if 'Animal' in actions:
            continue

        # MB Classification and Keywords search are done for all (question_code, rank)
        requests_responses = '''
        SELECT od.response,
            od.id_response_detail,
            od.question_code,
            od.id_user_coach,
            sq.RANK
        FROM erfw.odyssey_test_responses od
        LEFT JOIN business_area.SUBQUESTION sq on od.ID_SUBQUESTION = sq.ID_SUBQUESTION
        WHERE
            od.question_code = {0}
            AND sq.RANK = {1};
        '''.format(question, rank)

        responses_to_classify = connection.exec_data_frame(requests_responses)
        # num_to_classify = len(responses_to_classify)
        # num_full_batch = num_to_classify // BATCH_SIZE

        # for batch_index in range(num_full_batch + 1):
        # batch = responses_to_classify[batch_index*BATCH_SIZE:min((batch_index + 1)*BATCH_SIZE, num_to_classify)]
        # MB Classification
        print(question, rank)
        classified_responses = lc.data_2export(responses_to_classify, 'MB')
        print('finished MB classifications for question {0}, rank {1}'.format(question, rank))
        catalog.insert_from_data_frame('erfw', 'odyssey_test_responses_classified', reformat_df(classified_responses))
        connection.commit()

        # PLACEHOLDER FOR KEYWORDS CLASSIFICATION

        # CONNECTED

        # MEANING

        # EMOTION

        # NUDGE Classification if required only:
        if 'Nudge' in actions:

            classified_responses = lc.data_2export(responses_to_classify, 'Nudge')
            print('finished Nudge classifications for question {0}, rank {1}'.format(question, rank))
            catalog.insert_from_data_frame('erfw', 'odyssey_test_responses_classified', reformat_df(classified_responses))
            connection.commit()

finally:
    if connection:
        connection.close()
